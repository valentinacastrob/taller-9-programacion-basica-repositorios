# 2. Funcion definirPar(): Retorne una lista de True o False de acuerdo si el elemento de una lista que se pasa como argumento es par o no.
numerolista=int(input("Por favor ingrese un numero para la longitud de la lista:"))
def crearLista(L):
    lista=[]
    for u in range(0, L):
       lista.append(u)
    return lista
    
def verificarPar (A):
    lista=[]
    for u in A:
      if u%2 == 0:
         lista.append(True)
      else:
        lista.append(False)
    return lista

numeros=crearLista(numerolista)
pares=verificarPar(numeros)
print (numeros,pares)
    
