from math import*
from graphics import *

ventana= GraphWin ("simulador semiparabolico", 500, 400)
ventana.setCoords(0,0,600,200)


g=5
t=0
Vi=280
y=95
while y>=0:
    x=Vi*t
    y= y+(-1/2*g*t*t)
    print(x,y)
    circulo=Circle(Point(x,y) , 2)
    circulo.setFill("pink")
    circulo.draw(ventana)
    t=t+0.1
ventana.getMouse()
ventana.close()