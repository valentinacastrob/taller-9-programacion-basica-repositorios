#Taller: Generar un programa en python que dibuje en pantalla:
#1. Una linea:

import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

fig, ax = plt.subplots()

Path = mpath.Path
path_data = [
    (Path.MOVETO, (0.0, 0.0)),
    (Path.LINETO, (0.0, 0.0)),
    (Path.CURVE4, (5, 0.0)),
    ]
codes, verts = zip(*path_data)
path = mpath.Path(verts, codes)
x, y = zip(*path.vertices)
line, = ax.plot(x, y, 'go-')

ax.grid()
ax.axis('equal')
plt.show()

#2. elipse
import matplotlib.pyplot as plt
import matplotlib.patches as pc

fig1 = plt.figure() 
ax1 = fig1.add_subplot(111, aspect='equal')
plt.xlim(0, 1)
plt.ylim(0, 1)
ax1.add_patch(

    pc.Ellipse(  
        (0.5, 0.5),  
        0.6,  # Largo
        0.4,  # ancho  
        color='#8A2BE2'
    )
)
plt.show()