#Taller 24: Del repositorio de datos abiertos del distrito, leer el documento csv.
import csv
import pandas as pd
nombre_archivo = ("serviciosurgenciasambulanciasagosto2020.csv.csv")
filas=[]
#1. El listado de empresas que prestan el servicio de ambulancia:
print("----LISTADO DE EMPRESAS QUE PRESTAN EL SERVICIO DE AMBULANCIA----")
with open(nombre_archivo, "r") as archivo:
    lector = csv.reader(archivo, delimiter=";")
    # Omitir el encabezado
    next(lector, None)
    for fila in lector:
        nombre_prestador = fila[1]
        print(
            f"nombre_prestador: '{nombre_prestador}")
        
#2. La cantidad de registros (filas) que tiene el archivo:
print("----CANTIDAD COMPLETA DE DATOS/REGISTROS DEL ARCHIVO----")
with open(nombre_archivo, "r") as archivo:
    lector = csv.reader(archivo, delimiter=";")
    for fila in lector:
        filas.append(fila)
        numero_de_filas = fila[0]
        
        print(
            f"numero_de_filas: '{numero_de_filas}")
#3. las primeras 5 filas:

print("----PRIMERAS 5 FILAS SIN ENCABEZADO----")
print(filas[1])
print(filas[2])
print(filas[3])
print(filas[4])
print(filas[5])
#4. La dirección de SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.
print("----LA DIRECCION DE SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E. ES: ----")
pdfile = pd.read_csv("serviciosurgenciasambulanciasagosto2020.csv.csv", sep = ";", encoding = "ISO-8859-1")
print("Direcciones de la sede SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E.\n",pdfile[pdfile["nombre_prestador"] == "SUBRED INTEGRADA DE SERVICIOS DE SALUD SUR E.S.E"].direccion.unique(),'\n\n')
#5. El listado de prestadores de servicio cuyo nombre contenga la palabra "salud".
print("----LISTADO DE EMPRESAS CON LA PALABRA (SALUD)----")

print(pdfile[pdfile["nombre_prestador"].str.contains("salud", case = False)])