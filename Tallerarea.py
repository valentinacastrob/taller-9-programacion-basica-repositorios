from pylab import *
import matplotlib.pyplot as plt

y=str(input("f(x)="))
x=np.linspace (0, 10)
def f(x):
    return round (eval(y), 2)
x1=float(input("ingrese la coordenada de x1:"))
x2=float(input("ingrese la coordenada de x2:"))
dominio=int(input("ingrese el limite del dominio:"))
a=(x1, 0) 
b=(x2, 0)

y1=f(x1)
y2=f(x2) 

b1=np.sqrt(((x2-x1)**2))
b2=np.sqrt(((y2-y1)**2))
area=y1+((b1+b2)/2)
print("area=", area)
print("las coordenadas son:", (x1,y1),(x2, y2), a,b)
ejex=[]
ejey=[]

for i in range (int(x1), dominio):
    ejex.append(i)
    ejey.append(f(i))
    
x=[x1, x1, x2, x2, x1]
y=[0, y1, y2, 0, 0]
plt.plot(x, y)
plt.plot(ejex, ejey)
plt.fill_between(x, y)
plt.show()
