#Elaborar un programa, que lea el archivo y contenga un conjunto de funciones que:
#a.Calcule el valor promedio por metro cuadrado de un municipio dado, es decir, el nombre del municipio es un parámetro de la función.
print('a.Calcule el valor promedio por metro cuadrado de un municipio dado, es decir,\n el nombre del municipio es un parámetro de la función.')
import csv
from enum import unique
lista=[]
with open(file='tablainformacion.csv', mode = 'r', encoding = 'utf-8') as archivo:
  
    csv_reader=csv.reader(archivo,delimiter=";")
    line_count=0
    for row in csv_reader:
        if line_count>0:
            predio={"codPredio":row[0],"area":row[1],"municipio":row[2],"precio":row[3],"tipo":row[4]}
            lista.append(predio)
        line_count+=1
    dicMunicipio={}
    for p in lista:
        if p ["municipio"] in dicMunicipio:dicMunicipio[p["municipio"]]["area"]+=p["area"]; dicMunicipio[p["municipio"]]["precio"]+=p["precio"]
    else:
        dicMunicipio[p["municipio"]]={"area":p["area"], "precio":p["precio"]}
    for k,v in dicMunicipio.items():
        precio=v["precio"]
        area=v['area']
        promedio=int(precio)/int(area)
        print("municipio: "+k+"valor promedio m2 "+str(promedio))
#b. Retorne una lista ordenada de precios para un municipio y tipo dado, es decir, el municipio y el tipo son parámetros de la función.
print('Retorne una lista ordenada de precios para un municipio y tipo dado, es decir,\n el municipio y el tipo son parámetros de la función.')
for fila in csv_reader:
          precio = fila[3]
          Municipio = fila[2]
          tipo = fila[4]
          print(
            f"precio: {precio} , municipio: {Municipio}, tipo:  {tipo}")
#c.Retorne una lista ordenada de los precios que sean iguales o mayores a un valor dado para un municipio y tipo específico. Tanto el valor, como el nombre de municipio y tipo son parámetros de la función.
print('c.Retorne una lista ordenada de los precios que sean iguales o mayores a un valor dado para un municipio y tipo específico.\n Tanto el valor, como el nombre de municipio y tipo son parámetros de la función.')
import pandas as pd

data = pd.read_csv('tablainformacion.csv',encoding='iso-8859-1')
def lista_de_predios(mun:str, tipo:str, ret:int = 1):

  selected = lista_de_predios(mun, tipo, 2)
  selected = selected[selected['Precio'] >= precio]

  return list(selected.Precio)
def lista_de_predios_umbral(mun:str, tipo:str, precio:int):
 print(lista_de_predios_umbral(
  input('ingrese el municipio: '),
  input('ingrese el tipo: '),
  int(input('ingrese el valor: '))
))

#d. Retorne el promedio del área de todos los predios de la tabla.
print('d. Retorne el promedio del área de todos los predios de la tabla.')
def promedio_area():
  return sum(list(data.Área))/len(list(data.Área))
print(promedio_area())
#e.Retorne una lista ordenada de los municipios que tienen predios registrados en la tabla. Los nombres no pueden ir repetidos.
print('e.Retorne una lista ordenada de los municipios que tienen predios registrados en la tabla. Los nombres no pueden ir repetidos.')
datos=pd.read_csv('tablainformacion.csv',header=0)
municipios=datos['Municipios']
print(municipios)

#f.Retorne una tupla con las áreas de los predios de un tipo dado. El tipo es un parámetro de la función.
print('f.Retorne una tupla con las áreas de los predios de un tipo dado. El tipo es un parámetro de la función.')
def lista_areas(tipo:str):
      return list(data[data.Tipo == tipo].Área.unique())
print(lista_areas(input('digite el tipo: '))) 

#g.Retorne un diccionario con los datos completos de un predio dado. El código del predio es un parámetro de la función.