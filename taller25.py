print("----Bienvenido al curso de cadenas----")
print('conceptos importantes:')
print("1.string=cadena de texto (alfanumericos)\n2.str():convertir un valor a su correspondiente representacion como string.\n3.Existen caracteres especiales que son \ acompañados de alguna letra")
print("Empezaremos a explicar las diferentes funciones que tenemos para cadenas.")
#Función len
print('--Si quisieramos saber la longitud de una cadena nos basta con usar (len) y lo hariamos de la siguiente forma:\na=len(cadena0)')
cadena0=input("INGRESA UNA CADENA DE TEXTO:")
print('y procedemos a imprimir; print(a) y esto nos da:')
a= len(cadena0)
print(a)
#Función concatenar y +
print('--Segunda función. Esta nos sirve para sumar cadenas y utilizamos (+) de la siguiente forma:\nprint(cadena+str(cadena2)\nSolicitamos dos cadenas:')
cadena1=input("INGRESA UNA CADENA DE TEXTO:")
cadena2=input("INGRESA UNA CADENA DE NÚMEROS:")
print('y nos queda:')
print(cadena1+str(cadena2))
print('como se menciona anteriormente el (str) se usa en este caso porque es una cadena numerica y necesitamos su representación en string')
#Función upper
print('--Tercera función. La función upper() nos devolvera la cadena en mayusculas y la utilizamos de la siguiente forma:\nprint(cadena.upper())')
cadena3=input("INGRESA UNA CADENA DE TEXTO EN MINÚSCULA:")
print('que nos dara como resultado:')
print(cadena3.upper())
#Función lower
print('y ¿qué pasa si queremos nuestra cadena en minusculas?\n--Utilizamos la función lower() de la misma forma que upper y nos queda\nprint(cadena.lower())')
cadena4=input("INGRESA UNA CADENA DE TEXTO EN MAYUSCULA:")
print('Y con la función lower quedaría:')
print(cadena4.lower())
#Función Strip
print('--Cuarta función. Lafunción Strip() nos permite quitar los espacios en blanco al principio y final de una cadena y se utiliza así:\nprint(cadena5.strip())')
cadena5=input("INGRESA UNA CADENA DE TEXTO:")
print('Y quedaría:')
print(cadena5.strip())
#Función Startswich
print('--Quinta función. Esta función se usa para verificar si una oración determinada comienza con una cadena en particular.')
print('tenemos esta oración de ejemplo: str = "cadena de ejemplo" y plantearemos los parametros:\nprint (str.startswith( cadena ));\nprint (str.startswith( de, 7, 9 ));\nprint (str.startswith( cadena, 2, 4 ));')
print('y nos da:')
str = "cadena de ejemplo";
print (str.startswith( 'cadena' ));
print (str.startswith( 'de', 7, 9 ));
print (str.startswith( 'cadena', 2, 4 ))
#Función Endswich
print('--Sexta función. Esta función es igual solo que preguntamos si termina con una cadena en particular.\nprint(str.endswith("ejemplo"))\nprint(str.endswith("de", 7, 9)) \nprint(str.endswith("cadena", 1, 7))')
print('y nos da:')
print(str.endswith("ejemplo")) 
print(str.endswith("de", 5, 9)) 
print(str.endswith("cadena", 1, 7)) 
print('los números los utilizamos para determinar la ubicacion de la cadena que solicitamos')
#Función find
print('--Septima función. Esta función nos da la ubicación de la palabra que estamos solicitando\ncadena6=Si la vida fuera estable todo el tiempo, yo no bebería ni malgastaría la plata')
print('usamos find: cadena6.find() y la palabra que queremos buscar: print(cadena6.find(tiempo))\ny nos da:')
cadena6="Si la vida fuera estable todo el tiempo, yo no bebería ni malgastaría la plata"
print(cadena6.find('tiempo'))
#Función replace
print('--Octava función. Esta función nos permite cambiar una cadena(palabra) en especifico por otra\nejemplo: cadena7 = Por un abrazo de la flaca daría lo que fuera')
print("y cambiaremos la palabra (abrazo) por (beso) así:\nnueva = cadena7.replace('abrazo', 'beso')\n Y procedemos a imprimir print(nueva)")
cadena7 = 'Por un abrazo de la flaca daría lo que fuera'
nueva = cadena7.replace('abrazo', 'beso')
print(nueva)
#Función split
print('--Novena función. Esta función rompe la cadena')
cadena8=input("INGRESA UNA CADENA DE TEXTO:")
print('luego de ingresarla la rompe así:')
print(cadena8.split())
