#Diccionario con 10 items:
miDiccionario = {
    "item1" : 2,
    "item2" : 5,
    "item3" : 6,
    "item4" : 2,
    "item5" : 6,
    "item6" : 12,
    "item7" : 15,
    "item8" : 4,
    "item9" : 13,
    "item10" : 8,
    }
#a. Multiplicación de todos los valores. Deben recorrer el diccionario.
print("Punto A Multiplicación de todos los valores:")
resultado=1
for a in miDiccionario.values():
    resultado = resultado*a
print(resultado)

#b. Valores duplicados.
print("Punto B Valores duplicados:")
for b in miDiccionario.values():
    contador=0
    for c in miDiccionario.values():
        if c==b:
            contador=contador+1
    if contador>1:
        print(b)


#c.Scrip que elimine del diccionario los valores que sean iguales o múltiplos de un número ingresado por el usuario.
print("Punto C:")
resultado={}
e=int(input("ingrese un numero: "))
for f in miDiccionario:
    if miDiccionario[f]!=e:
     resultado.setdefault(f, miDiccionario[f])
print(resultado)


#2.Crear un script de python que, dados dos diccionarios, genere un tercero con la suma de los items que tengan la misma llave.
print("segundo punto Suma de items con misma llave:")
miDiccionario1 = {
    "valorA" : 2,
    "valorB" : 6,
    "valorC" : 4,
    }
miDiccionario2 = {
    "valorA" : 1,
    "valorB" : 3,
    "valorC" : 10,
    }
keys = []
valores = []

for i in miDiccionario1:
    for p in miDiccionario2:
        if i == p:
            keys.append(p)
            valor = miDiccionario1.get(i) + miDiccionario2.get(p)
            valores.append(valor)

diccionario3 = {keys[i]: valores[i] for i in range(len(keys))}

print(diccionario3)


#3. Teniendo un diccionario cuyo items tienen como valor un conjunto de letras en tuplas:
# a.script de python que imprima todas las posibles combinaciones de letras que se generen al combinar los elementos de cada tupla.
